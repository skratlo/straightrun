(ns server
  (:require
   [bidi.bidi :as bidi]
   [straightrun.handler :as handler]
   [straightrun.routes :as routes]))

(defn handler [request]
  (let [match   (bidi/match-route routes/routes (:uri request))
        page-id (:handler match)
        params  (:route-params match)]
    (if match
      {:status  200
       :headers {"Content-Type" "text/html"}
       :body    (handler/render-page page-id params)}
      {:status  404})))
