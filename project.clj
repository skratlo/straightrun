(defproject straightrun "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.9.0-beta4"]
                 [org.clojure/clojurescript "1.9.946"]
                 [org.clojure/core.async  "0.3.443"]
                 [venantius/accountant "0.2.3"]
                 [ring/ring-defaults "0.3.1"]
                 [ring "1.6.3"]
                 [reagent-utils "0.3.0"]
                 [reagent "0.8.1"]
                 [bidi "2.1.3"]
                 [hiccup "1.0.5"]]

  :plugins [[lein-figwheel "0.5.16"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-ring "0.12.3"]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["src"]

                :figwheel {:on-jsload "straightrun.core/on-js-reload"
                           :open-urls ["http://localhost:3449/"]}

                :compiler {:main straightrun.core
                           :asset-path "/js/compiled/out"
                           :output-to "resources/public/js/compiled/straightrun.js"
                           :output-dir "resources/public/js/compiled/out"
                           :source-map-timestamp true
                           :preloads [devtools.preload]}}
               ;; --
               {:id "min"
                :source-paths ["src"]
                :compiler {:output-to "resources/public/js/compiled/straightrun.js"
                           :output-dir "resources/public/js/compiled/out-min"
                           :source-map "resources/public/js/compiled/straightrun.js.map"
                           :main straightrun.core
                           :optimizations :advanced
                           :fn-invoke-direct true
                           :elide-asserts true
                           :compiler-stats true
                           :parallel-build true
                           :pretty-print false}}]}

  :figwheel {:css-dirs ["resources/public/css"] ;; watch and update CSS
             :nrepl-port 7888
             :ring-handler server/handler}

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.4"]
                                  [figwheel-sidecar "0.5.16"]
                                  [cider/piggieback "0.3.9"]]
                   :source-paths ["src" "dev"]
                   :repl-options {:nrepl-middleware [cider.piggieback/wrap-cljs-repl]}
                   :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                                     :target-path]}})
