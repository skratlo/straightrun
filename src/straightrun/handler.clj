(ns straightrun.handler
  (:require [hiccup.page :refer [html5]]
            [straightrun.pages :as pages]))

(defn normalize [component]
  (if (map? (second component))
    component
    (into [(first component) {}] (rest component))))

(defn fn?* [x]
  (or (fn? x)
      (instance? clojure.lang.MultiFn x)))

(defn render
  [component]
  (cond
    (fn?* component)
    (render (component))

    (not (coll? component))
    component

    (coll? (first component))
    (map render component)

    (keyword? (first component))
    (let [[tag opts & body] (normalize component)]
      (->> body
           (map render)
           (into [tag opts])))

    (fn?* (first component))
    (render (apply (first component) (rest component)))))

(defn render-page
  [page-id params]
  (->> [pages/page-pure page-id params]
       render vector pages/top-level html5))
