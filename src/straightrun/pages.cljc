(ns straightrun.pages
  (:require
   [straightrun.pages.core :as pages]
   [straightrun.pages.index]
   [straightrun.pages.blog]
   [straightrun.util :refer [link-to]]
   #?(:cljs
      [reagent.session :as session])))

(defn top-level
  ([content]
   [:html
    [:head
     [:title "Straightrun Consulting"]
     [:meta {:charset "UTF-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
     [:link {:rel "stylesheet" :href "https://unpkg.com/spectre.css/dist/spectre.min.css"}]
     [:link {:rel "stylesheet" :href "https://unpkg.com/spectre.css/dist/spectre-exp.min.css"}]
     [:link {:rel "stylesheet" :href "https://unpkg.com/spectre.css/dist/spectre-icons.min.css"}]
     [:link {:rel "stylesheet" :href "https://fonts.googleapis.com/css?family=Roboto+Slab|Titillium+Web"}]
     [:link {:rel "stylesheet" :href "/css/style.css"}]]
    [:body
     (into [:div#site] content)
     [:script {:type "text/javascript" :src "/js/compiled/straightrun.js"}]
     [:script {:type "text/javascript"}
      "straightrun.core.init_BANG_();"]]])
  ([]
   (top-level [])))

(defn menu []
  [:nav {:role "navigation"}
   [:div#menu-toggle
    [:input {:type "checkbox"}]
    [:span]
    [:span]
    [:span]
    [:ul#menu
     (link-to [:li "Home"] [:index])
     (link-to [:li "Blog"] [:blog])
     (link-to [:li "Contact"] "#contact" [:index])]]])

(defn page-pure
  [page-id params]
  [:div {:class (name page-id)}
   [menu]
   [:section
    [pages/page-contents page-id params]]])

#?(:cljs
   (defn page []
     (fn []
       (let [route (session/get :route)]
         [page-pure (:current-page route) (:route-params route)]))))
