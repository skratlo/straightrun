(ns ^:figwheel-hooks straightrun.core
  (:require [accountant.core :as accountant]
            [bidi.bidi :as bidi]
            [clojure.string :as str]
            [reagent.core :as reagent :refer [atom]]
            [reagent.dom :as dom]
            [reagent.session :as session]
            ;; --
            [straightrun.pages :as pages]
            [straightrun.routes :as routes]))

(enable-console-print!)

(defn ^:after-load on-js-reload []
  (reagent/render-component
   [pages/page (:current-page (session/get :route))]
   (. js/document (getElementById "site"))))

(defn- scroll-into-view [q]
  (js/setTimeout
   #(-> (. js/document (querySelector q))
        (.scrollIntoView)) 0))

(defn ^:export init! []
  (accountant/configure-navigation!
   {:nav-handler (fn [path]
                   (let [match (bidi/match-route routes/routes path)
                         current-page (:handler match)
                         route-params (:route-params match)]
                     (when-let [hash (last (re-seq #"#.+$" path))]
                       (scroll-into-view hash))
                     (session/put! :route {:current-page current-page
                                           :route-params route-params})))
    :path-exists? (comp boolean (partial bidi/match-route routes/routes))})
  (accountant/dispatch-current!)
  (on-js-reload))
