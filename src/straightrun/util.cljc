(ns straightrun.util
  (:require
   [bidi.bidi :as bidi]
   [straightrun.routes :as routes]
   #?@(:cljs
       [[accountant.core :as accountant]
        [goog.object :as gobj]])))

#? (:cljs
    (defn hide-menu! []
      (gobj/set
       (. js/document (querySelector "#menu-toggle input"))
       "checked" false)))

(defn link-to
  ([label hash path-for-args]
   (let [path (apply bidi/path-for routes/routes path-for-args)
         path (if (some? hash) (str path hash) path)]
     [:a (merge {:href path}
                #?(:cljs {:on-click #(do (hide-menu!)
                                         (accountant/navigate! path))}))
      label]))
  ([label path-for-args]
   (link-to label nil path-for-args)))

(defn block [& content]
  [:div.container.grid-xl
   [:div.columns
    [:div.column.col-2.hide-md]
    (into [:div.column.col-8-xl.col-12-md.main] content)
    [:div.column.col-2.hide-md]]])
