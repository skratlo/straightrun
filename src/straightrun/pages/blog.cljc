(ns straightrun.pages.blog
  (:require
   #?(:clj [clojure.java.io :as io])
   #?@(:cljs [[reagent.ratom :as ratom]
              [goog.net.XhrIo :as xhr]])
   [clojure.string :as str]
   [markdown.core :as markdown]
   [straightrun.pages.core :as pages]
   [straightrun.util :refer [block link-to]])
  #?(:cljs
     (:require-macros
      [straightrun.pages.blog :refer [list-posts]])))

(def blog-posts-root
  "resources/public/blog")

(def blog-posts-uri-root
  "/blog/")

(defn parse-post-meta [meta]
  (-> meta
      (update :title first)
      (update :date first)
      (as-> $ (if (:tags $)
                (update $ :tags #(str/split (first %) #"\s"))
                $))))

#?(:clj
   (defmacro list-posts []
     (->>
      (for [f (file-seq (io/file blog-posts-root))
            :when (.isFile f)
            :let [fname (.getName f)
                  [[_ id]] (re-seq #"(.+)\.md$" fname)]]
        (-> (markdown/parse-metadata f)
            (assoc :path fname :id id)
            (parse-post-meta)))
      (sort-by :id)
      (reverse)
      (vec))))

(def posts
  (list-posts))

(def posts-map
  (reduce (fn [m p] (assoc m (:id p) p)) {} posts))

(defn posts-list []
  [:div.timeline
   (for [p posts]
     ^{:key (:path p)}
     [:div.timeline-item {:id (:id p)}
      [:div.timeline-left
       [:a.timeline-icon {:href (str "#" (:id p))}]]
      [:div.timeline-content
       [:div.tile.tile-centered
        [:div.tile-content
         [:div.tile-title
          [link-to (:title p)
           [:blog-post :id (:id p)]]
          " / " (:date p)]
         [:div.tile-subtitle
          ]]]]])])

(defmethod pages/page-contents :blog [& args]
  [:div
   [:div.intro
    [block
     [:h1 "Straightrun Blog / ✍"]
     [:p "Musings and writings about art of programming..."]]]
   [:div.posts-list
    [block
     [posts-list]]]])

#?(:cljs
   (def blog-post-ref
     (ratom/atom nil)))

#?(:cljs
   (when-let [el (. js/document (querySelector "#blog-post-content"))]
     (let [[[_ id]] (re-seq #"([^/]+)\.html$" (.-pathname js/location))
           html (.-innerHTML el)]
       (reset! blog-post-ref {id html}))))

#?(:cljs
   (defn load-blog-post! [id]
     (xhr/send
      (str blog-posts-uri-root "/" id ".md")
      (fn [e]
        (let [html (markdown/md->html-with-meta (.. e -target getResponseText))]
          (reset! blog-post-ref {id (:html html)}))))))

#?(:clj
   (defn blog-post-html [id]
     [:div (:html (markdown/md-to-html-string-with-meta
                   (slurp (str blog-posts-root "/" id ".md"))))])
   :cljs
   (defn blog-post-html [id]
     (or (some-> (get @blog-post-ref id)
                 (as-> $ [:div {:dangerouslySetInnerHTML {:__html $}}]))
         [:p "Loading..."])))

(defmethod pages/page-contents :blog-post
  [_ {:keys [id]}]
  (let [p (get posts-map id)]
    #?(:cljs
       (when-not (contains? @blog-post-ref id)
         (load-blog-post! id)))
    [:div
     [:div.intro
      [block
       [:h1 (:title p)]
       [:p "( " (:date p) " )"]]]
     [:div
      [block
       [:div#blog-post-content
        (blog-post-html id)]]]]))
