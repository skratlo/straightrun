(ns straightrun.pages.core
  (:require [clojure.string :as str]))

(defmulti page-contents (fn [id params] id))

(defmethod page-contents :default
  [& args]
  [:div#not-found
   [:div.intro
    [:div.container.grid-lg
     [:div.columns
      [:div.col-3.hide-sm]
      [:div.col-6-lg.col-12-sm.main
       [:h1 "it is a... 404 / " (str/join ", " args)]]
      [:div.col-3.hide-sm]]]]
   [:div.rest
    [:div.container.grid-lg
     [:div.columns
      [:div.col-3.hide-sm]
      [:div.col-6-lg.col-12-sm.main
       [:p.text-center [:img {:src "/images/404.jpg"}]]]
      [:div.col-3.hide-sm]]]]])
