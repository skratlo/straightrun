(ns straightrun.pages.index
  (:require [straightrun.pages.core :as pages]
            [straightrun.util :refer [block]]))

(defmethod pages/page-contents :index
  [_ & args]
  [:div
   [:div.intro
    [block
     [:h1 "Straightrun Consulting"]
     [:h4.subtitle "Software consulting and services"]
     [:p "Design, architecture and development of full-stack web applications and
        solutions to help you ease and automate your domain-specific problems.
        We can analyze your problem space and with the help of your domain
        experts, identify problems that custom software solution can solve."]]]
   [:div.web-apps
    [block
     [:h4 "Web applications"]
     [:p "Custom tailored rich and interactive web applications that will make
        your company's processes more efficient and more fun. With years of
        experience we are confident to deliver solutions and get the job done."]
     [:h4 "User interfaces and experiences"]
     [:p "Single page applications (SPAs) with rich editing, content creation and
        data visualization tools. Be it tabular or n-dimensional data your users
        need to create, save and visualize, we got you covered with creative
        and innovative user controls."]
     [:h4 "Data modeling"]
     [:p "With thorough analysis of your problem domain we can design data model
        that precisely matches the solution you need. Well designed data model
        is the single most important part of any successful solution."]
     [:h4 "3D & 2D graphics, procedural geometry"]
     [:p "We can provide you with expertise gained from experience with working
        in interactive architectural 2D & 3D visualization and content creation
        tools. Be it custom designed rendering engines that can work anywhere
        from desktop to web browsers, or procedurally generated parametric
        3D models or 2D shapes, we can deliver. Got an idea for interactive
        product customization tools with real-time visual output? Yes we can.
        Have an architectural project that you need to present on the web or in
        VR? Let's talk."]]]
   [:div.rest
    [block
     [:h4 "About me"]
     [:p "Self-taught quality-oriented software developer. I have studied civil
        engineering and was always attracted to worlds of 3D visualization,
        source code and programming languages, and to innovative and rich
        user interfaces. In the early age I took passion in installing as much
        software as I could on my father's 386 PC and then trying it all. One
        day I installed something called " [:em "Java"] " and " [:em "Python"]
      ". I couldn't figure out what the former was for, but the latter gave me
        The Prompt (REPL) that seemed to be way better one than the one I knew
        from MS-DOS. Much later, I learned to use the coffee branded executable
        as well."]
     [:p "Tools I use: Clojure(Script), Pedestal, Figwheel, Reagent, Re-frame,
        HTML5, CSS, Java, Python, PostgreSQL, DataScript, Django, Celery,
        RabbitMQ, OpenGL, WebGL, Canvas, COLLADA, Blender, Linux, Amazon Web
        Services (AWS), React.js, Vue.js, and many, many more..."]
     [:hr]
     [:h4#contact "Contact form"]
     [:p "Goold old fashioned e-mail: " [:strong "dusan (at) struna.me"]]]]])
