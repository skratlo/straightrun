(ns straightrun.routes)

(def routes
  ["/" {"" :index
        "blog" :blog
        "blog/" {[:id ".html"] :blog-post}}])
